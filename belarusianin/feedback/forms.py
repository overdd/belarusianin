from django.forms import ModelForm, Textarea, forms
from captcha.fields import CaptchaField
from feedback.models import Contact

class ContactForm(ModelForm): # наша форма
    captcha = CaptchaField()
    class Meta:
        fields = "__all__"
        model = Contact
        widgets = {
            'text': Textarea(attrs={'cols': 65, 'rows': 10, }),
        }
