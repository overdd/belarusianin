from django.db import models
from django.forms import ModelForm, Textarea
from captcha.fields import CaptchaField

class Contact(models.Model): # тема, имя отправителя, мэйл, и само сообщение
    subject = models.CharField(max_length=100, verbose_name=u'Тема:')
    name = models.CharField(max_length=100, verbose_name=u'ФИО:')
    mail = models.EmailField(verbose_name=u'Mail:')
    text = models.TextField(verbose_name=u'Текст:')


    class Meta:
        ordering = ['name']
        verbose_name = 'Контактные данные'
        verbose_name_plural = 'Контактные данные'


    def __unicode__(self): # поиск в админке по "теме"
        return self.subject
