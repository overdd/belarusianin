from django.contrib import auth
from django.core.mail import send_mail
from django.shortcuts import render, render_to_response
from feedback.forms import ContactForm
from django.template import RequestContext
from django.http import Http404, HttpResponseRedirect, BadHeaderError, HttpResponse


def manage_feedback(request): # функция представления
    all_is_right = ''
    if request.method == 'POST': # пару проверок формы
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save() # сохраняем нашу форму в базу
            all_is_right = "Ваше сообщение успешно отправлено."
            form = ContactForm() # очищаем форму
            # return render_to_response('contacts.html')

    else:
        form = ContactForm()
    return render(request, 'feedback.html', {'form': form, 'all_is_right': all_is_right})


def manage_contacts(request):
    # if request.method == 'POST':
    #     return HttpResponseRedirect('/someurl/')
    if request.method == 'GET':
        return render_to_response('contacts.html')
    else:
        raise Http404()

