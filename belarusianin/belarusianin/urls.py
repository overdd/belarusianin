"""belarusianin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from app_blrs.views import index, test_db
from catalog.views import list_products, render_product_page
from cart.views import add_to_cart, remove_from_cart, cart_detail, update_quantity
from orders.views import create_order
from feedback.views import manage_feedback, manage_contacts


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tested/', test_db, {'title': 'This is title!', 'secstr': 'Second string'}),
    url(r'^$', index, name='index'),
    url(r'^catalog/', include('catalog.urls', namespace='catalog')),
    url(r'^cart/', include('cart.urls', namespace='cart')),
    url(r'^order/', include('orders.urls', namespace='orders')),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^feedback/', manage_feedback, name='feedback'),
    url(r'^contacts/', manage_contacts, name='contacts'),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^blockonomics/', include('blockonomics.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)