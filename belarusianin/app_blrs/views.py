from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
import datetime
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

from cart.forms import CartAddProductForm
from catalog.models import Category, Product, ProductAttribute
from orders.models import Order, OrderedProduct

# def test_db(request, title, secstr):    # функция представления
#     now = datetime.datetime.now()
#     html = "<html><body><h2>%s</h2><br>%s and now is %s</body></html>" % (title, secstr, now)
#     return redirect(to='http://tut.by')

def test_db(request, title, secstr):    # функция представления
    now = datetime.datetime.now()
    html = "<html><body><h2>%s</h2><br>%s and now is %s</body></html>" % (title, secstr, now)
    return HttpResponse(html)

def index(request, category_slug=None):
    category = Category.objects.all()
    template = loader.get_template('index.html')
    if category_slug:
        current_category = get_object_or_404(Category, slug=category_slug)
        products = Product.objects.filter(category=current_category)
    else:
        current_category = None
        products = Product.objects.all()
    context = {'category': category, 'products': products}
    return render(request, 'index.html', context)

# Это для Каталога

def list_products(request, category_slug=None):
    if category_slug:
        current_category = get_object_or_404(Category, slug=category_slug)
        products = Product.objects.filter(category=current_category)
    else:
        current_category = None
        products = Product.objects.all()
    return render(request, 'catalog/list.html', {
                  'category': current_category,
                  'products': products
    })


def render_product_page(request, id, product_slug, category_slug):
    print('Page opened!')
    current_category = get_object_or_404(Category, slug=category_slug)
    current_product = get_object_or_404(Product, id=id, slug=product_slug, category=current_category)
    add_to_cart_form = CartAddProductForm()
    return render(request, 'catalog/product_page.html',
                  {'product': current_product,
                   'cart_form': add_to_cart_form
                   })

