from django.contrib import admin
from catalog.models import Category, Product, ProductAttribute
from orders.models import Order, OrderedProduct
from accounts.models import MyProfile

admin.site.register(Order)
# admin.site.register(Product)
# admin.site.register(Category)
admin.site.register(ProductAttribute)
admin.site.register(OrderedProduct)
# admin.site.register(MyProfile)
