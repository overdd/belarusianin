from django.shortcuts import render
from .models import Article


def get_list_news(request):
    title = Article.objects.all()
    # date_of_news = Article.objects.all()
    context = {'articles': title}
    # context['articles'] = {'title': title, 'date_of_news': date_of_news}
    return render(request, 'articles.html', context)
