from django.conf.urls import url
from .views import get_list_news
urlpatterns = [
    url(r'^$', get_list_news),
]
