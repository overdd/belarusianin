from django.db import models


class Article(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    date_of_news = models.DateField(auto_now_add=True)
    image = models.ImageField(upload_to='news/images', blank=True, verbose_name='Изображение новости')
