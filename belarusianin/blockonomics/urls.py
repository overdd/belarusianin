from django.conf.urls import url
from . import views


#app_name = 'blockonomics'
urlpatterns = [
    url(r'^$', views.list_products, name='list_all_products'),
    # url(r'^(?P<category_slug>[-\w]*)/$', views.list_products, name='list_products_by_category'),
    # url(r'^(?P<category_slug>[-\w]+)/(?P<id>\d+)-(?P<product_slug>[-\w]+)/$',
    #     views.render_product_page, name='render_product')
]